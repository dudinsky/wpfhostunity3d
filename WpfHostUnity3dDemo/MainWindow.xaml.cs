﻿// // Copyright (c) Microsoft. All rights reserved.
// // Licensed under the MIT license. See LICENSE file in the project root for full license information.

using System;
using System.Windows;
using System.Windows.Input;
using System.Threading;
//using System.Runtime.InteropServices;
//using System.Windows.Interop;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Threading;

namespace WpfHostUnity3dDemo {
    using Process = System.Diagnostics.Process;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        String consoleInput = string.Empty;
        ObservableCollection<string> consoleOutput = new ObservableCollection<string>();// { "Console Emulation Sample..." };
        public event Action<String> onCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged( String propertyName ) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if( handler != null )
                handler( this, new PropertyChangedEventArgs( propertyName ) );
        }

        bool Nolog_ = false;
        public bool Nolog {
            get { return Nolog_; }
            set { if( Nolog_!=value ) { Nolog_=value; OnPropertyChanged( "Nolog" ); } }
        }
        bool RedirectStandardOutput_ = false;
        public bool RedirectStandardOutput {
            get { return RedirectStandardOutput_; }
            set { if( RedirectStandardOutput_!=value ) { RedirectStandardOutput_=value; OnPropertyChanged( "RedirectStandardOutput" ); } }
        }
        bool RedirectStandardError_ = false;
        public bool RedirectStandardError {
            get { return RedirectStandardError_; }
            set { if( RedirectStandardError_!=value ) { RedirectStandardError_=value; OnPropertyChanged( "RedirectStandardError" ); } }
        }
        bool RedirectStandardInput_ = false;
        public bool RedirectStandardInput {
            get { return RedirectStandardInput_; }
            set { if( RedirectStandardInput_!=value ) { RedirectStandardInput_=value; OnPropertyChanged( "RedirectStandardInput" ); } }
        }
        bool VisualStudioDebugOutput_ = false;
        public bool VisualStudioDebugOutput {
            get { return VisualStudioDebugOutput_; }
            set { if( VisualStudioDebugOutput_!=value ) { VisualStudioDebugOutput_=value; OnPropertyChanged( "VisualStudioDebugOutput_" ); } }
        }

        public String Unity3dExitCommand;
        public String ConsoleInput {
            get { return consoleInput; }
            set { consoleInput = value; OnPropertyChanged( "ConsoleInput" ); }
        }
        public ObservableCollection<String> ConsoleOutput {
            get { return consoleOutput; }
            set { consoleOutput = value; OnPropertyChanged( "ConsoleOutput" ); }
        }
        public void RunCommand() {
            string command = ConsoleInput;
            ConsoleOutput.Add( command );
            ConsoleInput = String.Empty;
            if( onCommand!=null )
                onCommand( command );
        }
        public MainWindow() {
            InitializeComponent();
        }

        private void On_UIReady(object sender, EventArgs e) {
            Unity3dControl.OnControlChanged += onLoad;
            Unity3dControl.OnCreate += onCreate;
            Unity3dControl.OnStart += onStart;
            Unity3dControl.OnExit += onExited;
            InputBlock.KeyDown += InputBlock_KeyDown;
            InputBlock.Focus();
            ResetSettings();
            System.Windows.Media.CompositionTarget.Rendering += UpdateOnRender;
        }

        void UpdateOnRender( object sender, EventArgs args ) {
            AddOutput();
        }

        void InputBlock_KeyDown( object sender, KeyEventArgs e ) {
            if( e.Key == Key.Enter ) {
                AddOutput();
                ConsoleInput = InputBlock.Text;
                RunCommand();
                InputBlock.Focus();
                ConScroller.ScrollToBottom();
            }
        }

        private void onLoad(bool loaded) {
            status.Text = loaded? "Loaded" : "Empty";
        }
        private void ChoosePath( object sender, EventArgs args ) {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = path.Text; // Default file name
            dlg.DefaultExt = ".exe"; // Default file extension
            dlg.Filter = "Exe files (.exe)|*.exe"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if( result==true ) {
                // Open document
                path.Text = dlg.FileName;
            }
        }

        List<String> messages = new List<String>();       
        void AddOutput() {
            if( messages.Count > 0 ) {
                lock( messages ) {
                    for( int i=0; i<messages.Count; ++i ) {
                        String msg = messages[i];
                        consoleOutput.Add( msg );
                        if( msg=="MainScript.OnApplicationQuit" ) {
                            do ; while( false );
                            StopProcess(null,null);
                            break;
                        }
                    }
                    messages.RemoveRange( 0, messages.Count );
                    ConScroller.ScrollToBottom();
                }
            }
        }

        DataReceivedEventHandler onDataHandler;
        private void onDataRecived( object sender, DataReceivedEventArgs data ) {
            String msg = data.Data;
            if( VisualStudioDebugOutput )
                System.Diagnostics.Debug.WriteLine( msg );
            lock( messages ) {
                messages.Add( msg );
            }
        }

        private void onCreate( Process process ) {
            process.StartInfo.RedirectStandardInput = RedirectStandardInput;
            process.StartInfo.RedirectStandardOutput = RedirectStandardOutput;
            process.StartInfo.RedirectStandardError = RedirectStandardError;
            if (RedirectStandardOutput) {
                onDataHandler = new DataReceivedEventHandler( onDataRecived );
                process.OutputDataReceived += onDataHandler;
            }
        }

        Process process_;
        private void onStart( Process process ) {
            if( RedirectStandardOutput ) {
                process.BeginOutputReadLine();
            }
            process_ = process;
        }
        private void ResetSettings(object sender, EventArgs args) {
            ResetSettings();
        }
        private void SaveSettings(object sender, EventArgs args) {
            Properties.Settings.Default.Unity3DExePath = path.Text;
            Properties.Settings.Default.Save();
        }
        private void ResetSettings() {
            path.Text = Properties.Settings.Default.Unity3DExePath;
        }

        private void StartProcess( object sender, EventArgs args ) {
            Unity3dControl.Path = path.Text;
            Unity3dControl.Start();
        }

        private void onExited(Process process) {
            if( onDataHandler!=null ) {
                process.CancelOutputRead();
                process.OutputDataReceived -= onDataHandler;
                onDataHandler = null;
            }
        }

        private void StopProcess( object sender, EventArgs args ) {
            Unity3dControl.Stop();
            process_ = null;
        }
    }
}

