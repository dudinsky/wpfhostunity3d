﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Diagnostics;

namespace WPFHostUnity3d {
    /// <summary>
    /// Interaction logic for HostUnity3DControl.xaml
    /// </summary>
    public partial class HostUnity3DControl : UserControl {
        private HostUnity3D _Control = null;
        public event Action<bool> OnControlChanged;

        public bool IsStarted { get { return _Control!=null; } }
        public Process Unity3dProcess { get { return _Control!=null? _Control.Unity3dProcess : null; } }  
        public String Path { get; set; }
        public String Arguments { get; set; }

        public event Action<Process> OnStart;
        public event Action<Process> OnCreate;
        public event Action<Process> OnExit;

        void OnChangedHandler(kStatus status, Process p) {
            if( status == kStatus.kStarted ) {
                if( OnStart!=null ) OnStart( p );    
            }
            else if (status == kStatus.kCreated) {
                if( OnCreate!=null ) OnCreate( p );
            }
            else if (status == kStatus.kExited) {
                if( OnExit!=null ) OnExit( p ); 
                Application.Current.Dispatcher.Invoke( ()=>{ Stop();  } );
            }
        }

        public HostUnity3DControl() {
            InitializeComponent();
        }

        public void Start() {
            if( _Control==null ) {
                var c = new HostUnity3D( Path, Arguments );
                c.OnChanged = OnChangedHandler;
                try {
                    Border.Child = c;
                }
                catch( Exception ex ) {
                    Border.Child = null;
                    c.OnChanged = null;
                    c.Dispose();
                    return;
                }
                _Control = c;
                if( OnControlChanged!=null )
                    OnControlChanged( IsStarted );
            }
        }
        public void Stop() {
            if( _Control!=null ) {
                var c = _Control;
                c.OnChanged = null;
                _Control = null;
                Border.Child = null;
                c.Dispose();
                if( OnControlChanged!=null )
                    OnControlChanged( IsStarted );
            }
        }
    }
}
