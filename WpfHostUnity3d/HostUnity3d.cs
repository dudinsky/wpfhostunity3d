﻿// // Copyright (c) Microsoft. All rights reserved.
// // Licensed under the MIT license. See LICENSE file in the project root for full license information.

#region Using directives

using System;
using System.Windows;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Diagnostics;

#endregion

namespace WPFHostUnity3d
{

    public enum kStatus { kCreated,kStarted,kExited }

    public class HostUnity3D : HwndHost
    {
        private Process _process = null;
        private bool isRunning = false;

        public IntPtr UnityHwnd { get; private set; }
        public String Path { get; private set; }
        public String Arguments { get; set; }
        public Process Unity3dProcess { get { return _process; } }

        public Action<kStatus,Process> OnChanged;

        public HostUnity3D( String path_, String arguments_ ) {
            UnityHwnd = IntPtr.Zero;
            Path = path_;
            Arguments = arguments_;
        }

        private void ProcessExited(object sender, EventArgs args) {
            UnityHwnd = IntPtr.Zero;
            _process.Exited -= ProcessExited;
            if( OnChanged != null )
                OnChanged( kStatus.kExited, _process );
			//do; while( false );
            //MessageBox.Show( "ProcessExited " );
        }

		private void NewUnity3dProcess( IntPtr hwndParent ) {
            if( _process==null ) {
				Debug.Assert( UnityHwnd==IntPtr.Zero );
                _process = new Process {
                    StartInfo = {
                        FileName = Path,
                        Arguments = this.Arguments + " -parentHWND " + hwndParent.ToInt32() + " -parentId " + Process.GetCurrentProcess().Id + " -nolog",//+ Environment.CommandLine,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardInput = false,
                        RedirectStandardOutput = false,
                        WindowStyle = ProcessWindowStyle.Hidden
                    }
                };
                _process.EnableRaisingEvents = true;
                _process.Exited += ProcessExited;
                if( OnChanged != null )
                    OnChanged( kStatus.kCreated, _process );
			}
		}


        private IntPtr StartUnity3D( IntPtr hwndParent ) {
            Debug.Assert( UnityHwnd==IntPtr.Zero );
            Debug.Assert( _process==null );
            NewUnity3dProcess( hwndParent );
            try {
                _process.Start();
                if( OnChanged != null )
                    OnChanged( kStatus.kStarted, _process );
                _process.WaitForInputIdle();
            }
            catch( Exception ex ) {
                _process = null;
                MessageBox.Show( ex.Message );
                return IntPtr.Zero;
            }
            // Doesn't work for some reason ?! IntPtr unityMainHWND = _process.MainWindowHandle;
            IntPtr unityHWND_ = IntPtr.Zero;
            EnumChildWindows( hwndParent, ( hwnd, lparam ) => { unityHWND_=hwnd; return 0; }, IntPtr.Zero );
            if( unityHWND_ == IntPtr.Zero ) 
                StopUnity3D();
            return unityHWND_;
        }

        private void StopUnity3D() {
            if( UnityHwnd != IntPtr.Zero ) {
                DeactivateUnityWindow();
                IntPtr unityMainHWND = _process.MainWindowHandle;
                bool destroyed = false;
                if( UnityHwnd != IntPtr.Zero ) {
                    destroyed = DestroyWindow( UnityHwnd );
                }
                UnityHwnd = IntPtr.Zero;
            }
            if( _process != null ) {
                try {
                    //_process.CloseMainWindow();
                    //_process.Close();
                    //_process.WaitForExit();
                    //Thread.Sleep( 10 );//while( _process.HasExited == false )
                    _process.Kill();
                }
                catch( Exception ex ) {
                    MessageBox.Show( ex.Message );
                }
            }
            
        }
        protected override HandleRef BuildWindowCore( HandleRef hwndParent ) {
            Debug.Assert( UnityHwnd==IntPtr.Zero );
            UnityHwnd = StartUnity3D( hwndParent.Handle );
            if( UnityHwnd!=IntPtr.Zero ) {
                ActivateUnityWindow();
            }
            return new HandleRef( this, UnityHwnd );
        }
        protected override void DestroyWindowCore( HandleRef hwnd ) {
            StopUnity3D();            
            DestroyWindow(hwnd.Handle);
        }

        private const int WM_ACTIVATE = 0x0006;
        private readonly IntPtr WA_ACTIVE = new IntPtr( 1 );
        private readonly IntPtr WA_INACTIVE = new IntPtr( 0 );

        private void ActivateUnityWindow() {
            SendMessage( UnityHwnd, WM_ACTIVATE, WA_ACTIVE, IntPtr.Zero );
        }
        private void DeactivateUnityWindow() {
            SendMessage( UnityHwnd, WM_ACTIVATE, WA_INACTIVE, IntPtr.Zero );
        }

        [DllImport("user32.dll", EntryPoint = "DestroyWindow", CharSet = CharSet.Unicode)]
        internal static extern bool DestroyWindow(IntPtr hwnd);
        internal delegate int WindowEnumProc( IntPtr hwnd, IntPtr lparam );
        [DllImport( "user32.dll" )]
        internal static extern bool EnumChildWindows( IntPtr hwnd, WindowEnumProc func, IntPtr lParam );
        [DllImport( "user32.dll" )]
        internal static extern int SendMessage( IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam );
        [DllImport( "User32.dll" )]
        internal static extern bool MoveWindow( IntPtr handle, int x, int y, int width, int height, bool redraw );
    }
}